---
layout: handbook-page-toc
title: Internal communications & team member engagement
description: "Our approach to internal communications and team member engagement at GitLab"
twitter_image: "/images/tweets/gitlab-first-look-twitter-card.png"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Internal communications is the connectitive tissue that keeps everything and everyone in the company moving forward in the same direction.

We aim to bring team members along on the journey through storytelling, empowering team members to become GitLab ambassadors and we seek input from across the company to continually iterate.

This page defines how our Internal Comms program operates.

## Guiding principles

1. 👁 Be credible, transparent, open, and honest 
1. 🤝 Provide context, reason, and a call to action.
1. 📈 Be consistent on delivery, cadence, and tone.
1. ⏱ Respect people’s time and attention.


## Working with Internal Communications

Given the amount of information we need to communicate with GitLab team members around the world it's important that we have a structured approach to communication.

In preparing key messages to be shared with all team members, we ask all parts of the business to follow the process below:

1. Share the need for internal comms support by answering the following questions in the #channel on slack
    1. Please note: this is a private channel for e-group and all-directs, if you do not have access please share your need with your department leader
1. In understanding the need, there will be a DRI from internal communications or the broader talent & engagement team assigned to the project.
1. A communications plan including release timing, cadence, and appropriate channels will be developed
    1. At least ~48 hours advanced notice is needed to plan a single message being shared
    1. For more complex internal communications requests, we request at least ~7-10 business days advanced notice to effectively collaborate together on a communications plan. We will opt to move more methodically depending on the scope and timeliness of the communication. 

## Your Role as an Internal Communications Partner

1. 📈 Take ownership of your subject matter expertise
1. 🤝 Give feedback effectively and assume positive intent
1. ⏱ Embrace change and write things down
1. 👣 Set a due date and embrace iteration
1. 👁 Say why, not just what

## Internal Communications and team member engagement channels

We use a variety of channels to communicate with various audiences within GitLab. The top channels we use and the purpose of each of these channels is as follows.

- **"While You Were Iterating" Newsletter:** Communicating new updates that are important for all team members to be aware of on a twice a month basis.
- **Manager README Monthly Newsletter:** Monthly proactive communication to people managers containing: what’s coming up, reminders of what’s important, guidance on team member talking points and what actions to take
- **Slack channels (#company-fyi and #company-fyi-private):** Timely important updates to all team members
- **GitLab Assembly:** Synchronous time to hear from GitLab leaders and answer team member questions
- **GitLab Handbook:** For permanent and non-confidential updates, we consider how information should live in the GitLab Handbook.

## Internal Communications planning

Timing of various messages and internal campaigns, and having a solid plan is a key to successful communication. As a company that's in growth mode, change is contant and we have a lot that needs to be shared with team members.

We plan when messages and campaigns will be live internally by maintaining an internal communications editorial calendar. 

Given this calendar tends to include information that is confidential until it's shared, it is not published here, but available for GitLab leaders looking to understand what is planned for a given week, month, or quarter.

Beyond timing, we want to be intentional about **what** we share with team members. Choices made about the weight of different themes we decide to highlight will have an impact on our culture. We use the general guideline below to guide our prioritization during normal times.

| Content topic | Percentage of Internal Share of Voice |
| ------------- | --------------------------------- |
| Team member total rewards and program updates (Primarily Finance & People Group) | ~30% share of voice |
| Values, culture, and team-building content  | ~20% share of voice |
| Leadership updates about key projects and functional changes | ~20% share of voice |
| Business outlook / Product updates | ~20% share of voice |
| Reactive, unplanned messaging (we know it happens!) | ~10% share of voice |

## Cascading information to various groups within GitLab

It's important that we consider how information will be received by various sub-groups within GitLab before sharing a piece of information with all team members. A typical cascade of information may end up crossing through various channels in cases where a significant change is being introduced. 

For example, if we were announcing a **big, exciting new program** we might cascade information as follows:

1. E-group provides input and feedback
1. People Group provided visibility to plan for team members questions
1. Managers receive information to ensure there is time to digest (#people-managers-and-above and Manager README)
1. All Team members receive the information with their managers and leaders already well-versed on the change (#company-fyi or #company-fyi-private and "While You Were Iterating..." newsletter)

It's important to note that information cascading could be seen as lacking transparency. That's not the case, rather it allows for managers and leaders to keep people more informed as they will have a chance to understand key changes with enough time to then answer questions to their teams.

## More to come

As we continue to grow our internal communications function, we'll continue to add more detailed information to this page. Please offer feedback by suggesting edits to this page or reaching out in the #talent-brand-engagement channel on slack.
